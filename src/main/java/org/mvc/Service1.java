package org.mvc;

import org.springframework.stereotype.Service;

/**
 * Created by Theja Varikuti on 16/08/20
 */
@Service
public class Service1 {

  public Integer add(Integer i1, Integer i2) {
    return i1+i2;
  }
}
