package org.mvc;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Theja Varikuti on 16/08/20
 */
@Controller
public class Controller1 {

  @Autowired
  private Service1 service1;

  @RequestMapping(value = "/add", method = GET, produces = APPLICATION_JSON_VALUE)
  public void add(@RequestParam Integer number1, @RequestParam Integer number2, HttpServletResponse response) throws Exception{
    response.setStatus(200);
    response.setContentType("text/json");
    response.getWriter().write("Result is : " + service1.add(number1, number2));
    response.getWriter().flush();
    response.getWriter().close();

  }
}
