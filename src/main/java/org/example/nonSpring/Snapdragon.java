package org.example.nonSpring;

import org.springframework.stereotype.Component;

/**
 * Created by Theja Varikuti on 16/08/20
 */
@Component
public class Snapdragon implements MobileProcessor {

  @Override
  public void process() {
    System.out.println("World's best CPU");
  }
}
