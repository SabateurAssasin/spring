package org.example.nonSpring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Theja Varikuti on 16/08/20
 */
@Component
public class Samsung {

  @Autowired
  private MobileProcessor mobileProcessor;

  public MobileProcessor getMobileProcessor() {
    return mobileProcessor;
  }

  public void setMobileProcessor(MobileProcessor mobileProcessor) {
    this.mobileProcessor = mobileProcessor;
  }

  public void config() {
    System.out.println("Configuration is Snapdragon 820, 4gb ram, Octa core");
    mobileProcessor.process();
  }
}
