package org.example.nonSpring;//import

/**
 * Created by Theja Varikuti on 16/08/20
 */
public class App {

  public static void main(String[] args) {

    // 1. Normal way of creating the object, but we dont want to do this way,
    // as we have to change the code later for other mobile phones
    // 2. Samsung object and Mobile processors are dependencies to this
    Samsung samsung = new Samsung();
    MobileProcessor mp = new Snapdragon();
    samsung.setMobileProcessor(mp);
    samsung.config();
  }
}
