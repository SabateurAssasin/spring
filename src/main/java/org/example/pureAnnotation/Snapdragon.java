package org.example.pureAnnotation;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * Created by Theja Varikuti on 16/08/20
 */
@Component
@Primary
public class Snapdragon implements MobileProcessor {

  @Override
  public void process() {
    System.out.println("World's best CPU");
  }
}
