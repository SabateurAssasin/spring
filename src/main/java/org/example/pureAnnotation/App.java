package org.example.pureAnnotation;//import

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by Theja Varikuti on 16/08/20
 */
public class App {

  public static void main(String[] args) {

    /**
     * Here we are creating the objects using spring as Annotations (@Component)
     * and as well as injecting them also using Annotations
     *
     * We are using Primary annotation if there are 2 classes getting referred and spring gets confused
     * We can also use @Qualifier annotation, for the same purpose
     */
    ApplicationContext factory2 = new AnnotationConfigApplicationContext(AppConfig.class);
    Samsung s2 = factory2.getBean(Samsung.class);
    s2.config();
  }
}
