package org.example.pureAnnotation;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Theja Varikuti on 16/08/20
 */
@Configuration
@ComponentScan(basePackages = "org.example.pureAnnotation")
public class AppConfig {

}
