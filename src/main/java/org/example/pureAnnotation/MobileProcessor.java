package org.example.pureAnnotation;

/**
 * Created by Theja Varikuti on 16/08/20
 */
public interface MobileProcessor {

  void process();
}
