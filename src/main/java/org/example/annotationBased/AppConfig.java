package org.example.annotationBased;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Theja Varikuti on 16/08/20
 */
@Configuration
public class AppConfig {

  @Bean
  public Samsung getPhone() {
    return new Samsung();
  }

  @Bean
  public MobileProcessor getProcessor() {
    return new Snapdragon();
  }
}
