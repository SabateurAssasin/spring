package org.example.annotationBased;//import

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by Theja Varikuti on 16/08/20
 */
public class App {

  public static void main(String[] args) {

    // We are using annotation to inject but not tp create the objects directly on its own
    // We will use this object as a dependency
    ApplicationContext factory = new AnnotationConfigApplicationContext(AppConfig.class);
    Samsung s2 = factory.getBean(Samsung.class);
    s2.config();
  }
}
