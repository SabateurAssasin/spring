package org.example.annotationBased;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Theja Varikuti on 16/08/20
 */
public class Samsung {

  @Autowired
  private MobileProcessor mobileProcessor;

  public MobileProcessor getMobileProcessor() {
    return mobileProcessor;
  }

  public void setMobileProcessor(MobileProcessor mobileProcessor) {
    this.mobileProcessor = mobileProcessor;
  }

  public void config() {
    System.out.println("Configuration is Snapdragon 820, 4gb ram, Octa core");
    mobileProcessor.process();
  }
}
