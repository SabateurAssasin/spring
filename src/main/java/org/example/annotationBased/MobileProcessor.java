package org.example.annotationBased;

/**
 * Created by Theja Varikuti on 16/08/20
 */
public interface MobileProcessor {

  void process();
}
