package org.example.xmlBased;//import

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Theja Varikuti on 16/08/20
 */
public class App {

  public static void main(String[] args) {

    // We are creating objects using xml based configurations and used
    // We are injecting processor of Samsung using setterInjection (constructorInjection also can be done the same way)
    ApplicationContext factory = new ClassPathXmlApplicationContext("spring.xml");
    Samsung s1 = (Samsung) factory.getBean("phone");
    s1.config();
  }
}
